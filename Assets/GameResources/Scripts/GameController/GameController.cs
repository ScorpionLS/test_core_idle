﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Контроллер для инициализации
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField]
    private List<FloatResource> floatResources = null;
    [SerializeField]
    private BuildingInfoView buildingView = null;
    [SerializeField]
    private EventSystem eventSystem = null;
    [SerializeField]
    private float startMoney=0;

    private RaycastHit hit;
    private Ray ray;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        for (int i = 0; i < floatResources.Count; i++)
        {
            floatResources[i].SetStartValue(startMoney);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!eventSystem.IsPointerOverGameObject())
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.TryGetComponent(out Building building))
                    {
                        buildingView.SetBuilding(building);
                    }
                }
            }
        }
    }
}