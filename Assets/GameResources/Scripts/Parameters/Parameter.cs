﻿using UnityEngine;

/// <summary>
/// Параметр
/// </summary>
[System.Serializable]
public class Parameter
{
    public Parameter(string name, string id, float startValue)
    {
        nameParametr = name;
        idParameter = id;
        value = startValue;
    }

    [SerializeField]
    private string nameParametr;

    public string ParametrName => nameParametr;

    [SerializeField]
    private string idParameter = string.Empty;
    public string Id => idParameter;

    [SerializeField]
    private float value=0;

    /// <summary>
    /// Параметра увеличения
    /// </summary>
    /// <returns></returns>
    public float GetValue() => value;

    public void AddValue(float val)
    {
        value += val;
    }
}