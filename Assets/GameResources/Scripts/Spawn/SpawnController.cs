﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер спавна клиентов
/// </summary>
public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private ExitPoint[] spawnPoints = null;
    [SerializeField]
    private List<BuildingPoint> buildingPoints = null;
    [SerializeField]
    private ClientPool clientPool = null;
    [SerializeField]
    private float waitToSpawn = 2f;

    private Coroutine spawnCoroutine = null;

    private void Start()
    {
        spawnCoroutine = StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(waitToSpawn);
            ExitPoint exitPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
            Client newClient = clientPool.GetObject();
            newClient.transform.position = exitPoint.transform.position;
            Transform pos = exitPoint.transform;
            newClient.SetData(exitPoint, buildingPoints);
        }
    }

    private void OnDestroy()
    {
        StopCoroutine(spawnCoroutine);
    }
}