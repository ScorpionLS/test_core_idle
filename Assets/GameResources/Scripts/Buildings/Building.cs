﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Абстрактный класс здания
/// </summary>
public class Building : MonoBehaviour
{
    public event Action OnUpdate = delegate { };
    [SerializeField]
    private string nameBuild = string.Empty;
    [SerializeField]
    private string idBuilding = string.Empty;
    [SerializeField]
    private int lvl = 1;
    [SerializeField]
    private MapUpgrade mapUpgrade = null;
    [SerializeField]
    private List<Service> allServices = null;
    /// <summary>
    /// Возвращает все параметры
    /// </summary>
    public List<Service> Getparameters() => allServices;

    /// <summary>
    /// Возвращает id здания
    /// </summary>
    public string GetId() => idBuilding;

    public int Clients { get; private set; }

    /// <summary>
    /// Возвращает имя здания
    /// </summary>
    /// <returns></returns>
    public string GetName() => nameBuild;

    /// <summary>
    /// Возвращает уровень здания
    /// </summary>
    public int GetLvl => lvl;

    public int GetMaxLvl()
    {
        if (mapUpgrade != null)
        {
            return mapUpgrade.GetCountLvl();
        }
        return lvl;
    }

    /// <summary>
    /// Возвращает карту улучшений
    /// </summary>
    public MapUpgrade GetMapUpgrade() => mapUpgrade;

    /// <summary>
    /// Улучшения здания
    /// </summary>
    /// <param name="level"></param>
    public virtual void Upgrade(LevelUpgrade level)
    {
        lvl++;

        for (int i = 0; i < level.GetParameters.Count; i++)
        {
            Parameter lvlParam = level.GetParameters[i];
            Service param = allServices.Find(x => x.Id.Equals(lvlParam.Id));
            if (param != null)
            {
                param.AddValue(lvlParam.GetValue());
            }
            else
            {
                Service newService = gameObject.AddComponent<Service>();
                newService.SetParameter(lvlParam.ParametrName, lvlParam.Id, lvlParam.GetValue());
                allServices.Add(newService);
            }
        }

        OnUpdate?.Invoke();
    }

    /// <summary>
    /// Работа с клиентом
    /// </summary>
    public virtual void SetClient(Client client)
    {
        Clients++;
        for(int i=0;i<allServices.Count;i++)
        {
            allServices[i].UsingService();
        }
        OnUpdate?.Invoke();
    }
}