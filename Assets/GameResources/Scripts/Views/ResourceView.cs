﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Вьюшка для отображения кол-ва ресурса
/// </summary>
public class ResourceView : MonoBehaviour
{
    [SerializeField]
    private Text infoText=null;
    [SerializeField]
    private BaseResource resource=null;

    private void Awake()
    {
        resource.OnUpdate += UpdateInfo;
    }

    private void UpdateInfo()
    {
        infoText.text = resource.GetStringResourceCount();
    }

    private void OnDestroy()
    {
        resource.OnUpdate -= UpdateInfo;
    }
}