﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Вьюшка для отображения инфо
/// </summary>
[RequireComponent(typeof(Text))]
public class ParameterView : MonoBehaviour
{
    private Text infoText;

    public void SetPrameter(Service parameter)
    {
        if(infoText==null)
        {
            infoText = GetComponent<Text>();
        }

        infoText.text ="Стоимость услиг : " + parameter.ServiceName + " : " + parameter.GetPrice().ToString();
    }
}
