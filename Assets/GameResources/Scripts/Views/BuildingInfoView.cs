﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// ВЬюшка здания
/// </summary>
public class BuildingInfoView : MonoBehaviour
{
    [SerializeField]
    private GameObject view = null;

    [SerializeField]
    private Text nameBuilding = null;
    [SerializeField]
    private Text lvlBuilging = null;
    [SerializeField]
    private Text clientCount = null;
    [SerializeField]
    private Text upgradeBtnText=null;

    [SerializeField]
    private Button upgradeBtn = null;
    private Building selectBuilding;

    [SerializeField]
    private GameObject parameterPanel=null;
    [SerializeField]
    private RectTransform rootPanel=null;
    [SerializeField]
    private FloatResource money =null;

    private List<GameObject> infoPanels = new List<GameObject>();
    private LevelUpgrade nextLvlBuilding;

    private void Awake()
    {
        money.OnUpdate += UpdateInfo;
    }

    public void SetBuilding(Building building)
    {
        if (building == null)
        {
            return;
        }
        if (selectBuilding != null)
        {
            UnSubscribe();
        }
        selectBuilding = building;
        selectBuilding.OnUpdate += UpdateInfo;
        view.SetActive(true);
        UpdateInfo();
    }

    private void UpdateInfo()
    {
        if(selectBuilding==null)
        {
            return;
        }
        Clear();
        nameBuilding.text = "Название : " + selectBuilding.GetName();
        lvlBuilging.text = "Уровень : " + selectBuilding.GetLvl.ToString() + " из " + selectBuilding.GetMaxLvl().ToString();
        clientCount.text = "Кол-во клиентов " + selectBuilding.Clients.ToString();
        nextLvlBuilding = GetNextLvl();
        upgradeBtn.interactable = nextLvlBuilding != null && IsUpgrades();

        upgradeBtnText.text = nextLvlBuilding != null ? nextLvlBuilding.Price.ToString() : "Max lvl";

        ShowParameters();
    }

    private void ShowParameters()
    {
        for(int i=0;i<selectBuilding.Getparameters().Count;i++)
        {
            GameObject panel = Instantiate(parameterPanel, rootPanel);
            panel.GetComponent<ParameterView>().SetPrameter(selectBuilding.Getparameters()[i]);
            infoPanels.Add(panel);
        }
    }

    private LevelUpgrade GetNextLvl()
    {
        MapUpgrade map = selectBuilding.GetMapUpgrade();
        if (map != null)
        {
            LevelUpgrade lvl = map.GetLevel(selectBuilding.GetLvl - 1);
            upgradeBtnText.text = lvl.Price.ToString();
            return lvl;
        }
        
        return null;
    }

    private bool IsUpgrades()
    {
        if(nextLvlBuilding!=null)
        {
            return money.GetResource() >= nextLvlBuilding.Price;
        }
        return false;
    }

    public void UpgradeBuilding()
    {
        LevelUpgrade lvl = nextLvlBuilding;
        selectBuilding.Upgrade(lvl);
        money.Del(lvl.Price);
    }

    private void Clear()
    {
        if (infoPanels.Count > 0)
        {
            for(int i=0;i< infoPanels.Count;i++)
            {
                Destroy(infoPanels[i]);
            }
            infoPanels.Clear();
        }
    }

    public void Close()
    {
        view.SetActive(false);
        UnSubscribe();
    }

    private void UnSubscribe()
    {
        if (selectBuilding != null)
        {
            selectBuilding.OnUpdate -= UpdateInfo;
        }
        selectBuilding = null;
        money.OnUpdate -= UpdateInfo;
    }

    private void OnDestroy()
    {
        if (view!=null && view.activeSelf)
        {
            UnSubscribe();
        }
    }
}
