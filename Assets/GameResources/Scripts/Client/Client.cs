﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Collections;
using System;
using Random = UnityEngine.Random;

/// <summary>
/// Класс клиента
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class Client : MonoBehaviour,IPoolObject<Client>
{
    private NavMeshAgent agent;
    private Vector3 targetPos;
    private bool isMove=false;

    private AbstractPoint startPoint;
    private AbstractPoint targetPoint;

    public event Action<Client> OnReturnToPool=delegate { };

    public void SetData(ExitPoint exitPoint,List<BuildingPoint> points)
    {
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }
        startPoint = exitPoint;
        targetPoint = points[Random.Range(0, points.Count)];
        targetPos = targetPoint.transform.position;
        isMove = true;
    }

    private void Update()
    {
        if(agent!=null && isMove)
        {
            agent.SetDestination(targetPos);
            if (agent.hasPath && agent.remainingDistance <= agent.stoppingDistance)
            {
                isMove = false;
                targetPoint.SetClient(this);
            }
        }
    }

    /// <summary>
    /// Возвращение обратно на точку появления
    /// </summary>
    public void Return()
    {
        targetPoint = startPoint;
        targetPos = targetPoint.transform.position;
        agent.SetDestination(targetPos);
        isMove = true;
    }

    /// <summary>
    /// Возврат в пул
    /// </summary>
    public void ReturnToPool()
    {
        OnReturnToPool?.Invoke(this);
    }
}
