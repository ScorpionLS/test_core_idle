﻿using UnityEngine;

/// <summary>
/// Оказываемый сервис
/// </summary>
public class Service : MonoBehaviour
{
    [SerializeField]
    private string nameService;

    [SerializeField]
    private string idService;

    [SerializeField]
    private float price;
    [SerializeField]
    private FloatResource resource=null;

    public void SetParameter(string name, string id, float startValue)
    {
        nameService = name;
        idService = id;
        price = startValue;
    }

    public string ServiceName => nameService;
    public string Id => idService;

    /// <summary>
    /// Параметра увеличения
    /// </summary>
    /// <returns></returns>
    public float GetPrice() => price;

    public void AddValue(float val)
    {
        price += val;
    }

    public void UsingService()
    {
        resource.Add(price);
    }
}