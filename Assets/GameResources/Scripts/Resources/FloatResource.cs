﻿using System;
using UnityEngine;

/// <summary>
/// Ресурс типа int
/// </summary>
[CreateAssetMenu(fileName = "New Float Resource", menuName = "Resources/Float resource")]
public class FloatResource : AbstractResource<float>
{
    public override event Action<bool> OnResult = delegate { };
    public override event Action OnUpdate = delegate { };

    public override void Add(float val)
    {
        value += val;
        OnUpdate?.Invoke();
    }

    public override void Del(float val)
    {
        if (IsEnought(val))
        {
            value -= val;
            OnResult?.Invoke(true);
            OnUpdate?.Invoke();
        }
        else
        {
            OnResult?.Invoke(false);
        }
    }

    public override float GetResource()
    {
        return value;
    }

    public override bool IsEnought(float val)
    {
        return value >= val;
    }

    public override string GetStringResourceCount()
    {
        return value.ToString();
    }

    public override void SetStartValue(float val)
    {
        value = val;
        OnUpdate?.Invoke();
    }
}