﻿/// <summary>
/// Асбтрактный класс ресурсов
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class AbstractResource<T> : BaseResource
{
    protected T value;
    public abstract void SetStartValue(T val);
    /// <summary>
    /// Добавляет ресурсы
    /// </summary>
    /// <param name="value">Кол-во ресурсов</param>
    public abstract void Add(T val);

    /// <summary>
    /// Удаляет ресурсы
    /// </summary>
    /// <param name="value">Кол-во ресурсов</param>
    public abstract void Del(T val);

    /// <summary>
    /// Проверка на то что хватает ресурсов
    /// </summary>
    public abstract bool IsEnought(T val);

    /// <summary>
    /// Возвращает кол-во ресурсов
    /// </summary>
    public abstract T GetResource();
}