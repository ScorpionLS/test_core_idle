﻿using UnityEngine;
using System;

/// <summary>
/// Базовый класс ресурсов
/// </summary>
public abstract class BaseResource : ScriptableObject
{
    [SerializeField]
    private string idResource = string.Empty;
    public string Id => idResource;

    public abstract event Action OnUpdate;
    public abstract event Action<bool> OnResult;
    /// <summary>
    /// Возврвщает строкое значение кол-ва ресурсов
    /// </summary>
    public abstract string GetStringResourceCount();
}
