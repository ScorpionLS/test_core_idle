﻿/// <summary>
/// Интерфейс объекта пула
/// </summary>
public interface IPool<T>
{
    /// <summary>
    /// Запрос объекта из пула
    /// </summary>
    /// <returns>Объект</returns>
    T GetObject();
}