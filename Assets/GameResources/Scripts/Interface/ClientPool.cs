﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Пул объектов врагов
/// </summary>
public class ClientPool : MonoBehaviour, IPool<Client>
{
    [SerializeField]
    private List<Client> prefabsClient=null;

    [SerializeField]
    private List<Client> clients = new List<Client>();

    /// <summary>
    /// Запрос на создание нового врага
    /// </summary>
    /// <returns>Новый враг</returns>
    public Client GetObject()
    {
        if (clients.Count > 0)
        {
            return GetPool();
        }
        return GetCreate();
    }

    private Client GetPool()
    {
        Client client = clients[Random.Range(0, clients.Count)];
        client.OnReturnToPool += ResetObject;
        clients.Remove(client);
        client.gameObject.SetActive(true);
        return client;
    }

    private Client GetCreate()
    {
        int rnd = Random.Range(0, prefabsClient.Count);
        Client newClient = Instantiate(prefabsClient[rnd], Vector3.zero,Quaternion.identity).GetComponent<Client>();
        newClient.OnReturnToPool += ResetObject;
        return newClient;
    }

    private void ResetObject(Client client)
    {
        client.OnReturnToPool -= ResetObject;
        client.gameObject.SetActive(false);
        clients.Add(client);
    }
}