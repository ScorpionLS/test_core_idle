﻿using System;
/// <summary>
/// Интерфейс объекта который хранится в пуле
/// </summary>
public interface IPoolObject<T>
{
    event Action<T> OnReturnToPool;
}