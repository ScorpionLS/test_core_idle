﻿/// <summary>
/// Понит возвращающий клиента в пул
/// </summary>
public class ExitPoint : AbstractPoint
{
    public override void SetClient(Client client)
    {
        client.ReturnToPool();
    }
}
