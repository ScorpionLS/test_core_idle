﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Асбтрактный класс точки к которой необходимо двигаться клиенту
/// </summary>
public abstract class AbstractPoint : MonoBehaviour
{
    public abstract void SetClient(Client client);
}
