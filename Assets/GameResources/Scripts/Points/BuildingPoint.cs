﻿using UnityEngine;

/// <summary>
/// Поинт здания
/// </summary>
public class BuildingPoint : AbstractPoint
{
    [SerializeField]
    private Building building=null;

    public override void SetClient(Client client)
    {
        building.SetClient(client);
        client.Return();
    }
}