﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Карта улучшения здания
/// </summary>
[CreateAssetMenu(fileName ="new map upgrade",menuName ="Upgrade/Building MapUpgrade")]
public class MapUpgrade : ScriptableObject
{
    [SerializeField]
    private List<LevelUpgrade> allUpgrade=null;

    /// <summary>
    /// Вовзращает общее кол-во уровней
    /// </summary>
    public int GetCountLvl() => allUpgrade.Count;

    /// <summary>
    /// Возвращает конкретный уровень
    /// </summary>
    public LevelUpgrade GetLevel(int id)
    {
        if (allUpgrade.Count>id && id>=0)
        {
            return allUpgrade[id];
        }
        return null;
    }
}