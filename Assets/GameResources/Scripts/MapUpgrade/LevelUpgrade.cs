﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Уровень улучшения
/// </summary>
[System.Serializable]
public class LevelUpgrade
{
    [SerializeField]
    private string name=string.Empty;
    [SerializeField]
    private int price=0;

    public int Price => price;

    [SerializeField]
    private List<Parameter> allUpgradeParameters=null;

    public List<Parameter> GetParameters => allUpgradeParameters;
}
